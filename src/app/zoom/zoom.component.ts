import { Component, OnInit } from '@angular/core';
import { ZoomMtg } from 'zoomus-jssdk';

ZoomMtg.setZoomJSLib('https://source.zoom.us/1.7.2/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

@Component({
  selector: 'app-zoom',
  templateUrl: './zoom.component.html',
  styleUrls: ['./zoom.component.scss']
})
export class ZoomComponent implements OnInit {
  meetConfig = {
    apiKey: 'AAKWHUSpQV2iT8HbnIN0NA',
    apiSecret: 'LOMxVQJzgsCwMlKamh6Q79Ax6HVn8PsA2aMJ',
    meetingNumber: 6063430212,
    userName: 'test',
    passWord: '',
    leaveUrl: 'http://localhost:4200',
    role: 0,
  };

  signature = ZoomMtg.generateSignature({
    meetingNumber: this.meetConfig.meetingNumber,
    apiKey: this.meetConfig.apiKey,
    apiSecret: this.meetConfig.apiSecret,
    role: this.meetConfig.role,
    success: (res) => {
      console.log(res.result);
    }
  });
  constructor() { }

  ngOnInit() {
 
    ZoomMtg.init({
      leaveUrl: this.meetConfig.leaveUrl,
      isSupportAV: true,
      success: (res) => {
        ZoomMtg.join({
          meetingNumber: this.meetConfig.meetingNumber,
          userName: this.meetConfig.userName,
          signature: this.signature,
          apiKey: this.meetConfig.apiKey,
          userEmail: '',
          passWord: this.meetConfig.passWord,
          success: (ms) => {
            console.log(ms, 'join meeting success');
          },
          error: (err) => {
            console.log(err);
          }
        });
      },
      error: (res) => {
        console.log(res);
      }
    });


  }

}
