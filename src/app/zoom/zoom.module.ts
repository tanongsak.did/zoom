import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZoomComponent } from './zoom.component';
import { ZoomRoutingModule } from './zoom.routing.module'
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [ZoomComponent],
  imports: [
    CommonModule,
    ZoomRoutingModule,
    HttpClientModule
  ]
})
export class ZoomModule { }
