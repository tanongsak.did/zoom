import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page.component';
import { PageRoutingModule } from './page.routing.module'
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    PageComponent,
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    HttpClientModule,
  ]
})
export class PageModule { }
